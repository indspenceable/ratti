﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputManager : MonoBehaviour
{
    public bool InputDisabled
    {
        get
        {
            return TextboxController.Instance.CurrentlyDisplaying || InventoryManager.Instance.IsOpen();
        }
    }
    public float Horizontal()
    {
        return Input.GetAxisRaw("Horizontal");
    }
    public float Vertical()
    {
        return Input.GetAxisRaw("Vertical");
    }
}
