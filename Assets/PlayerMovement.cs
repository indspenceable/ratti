﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Serialization;

public interface IInteractable
{
    void SetState(bool v);
    string DisplayString();
    void Interact();
    void UseItem(string item);
}
public enum InteractionHint
{
    NONE = 0,
    CHECK = 1,
}
public class PlayerMovement : MonoBehaviour
{
    [Header("Animations")]
    public Animator anim;
    public Vector2 Facing = new Vector2(0, -1);

    // TODO these will be removed at some point
    public float TransformBaseOffset;
    public Transform spriteTransform;
    //public Animator InteractableHint;
    [Header("Movement")]
    public float WalkSpeed;
    [Header("Layer Masks")]
    public LayerMask WallLayerMask;
    public LayerMask HotspotLayerMask;
    public LayerMask InteractablesMask;
    public PlayerInputManager playerInput;
    private InteractableNameDisplay interactableNameDisplay;


    bool MayTakeAction()
    {
        return !playerInput.InputDisabled;
    }

    Vector3 InteractPoint()
    {
        return transform.position + (Vector3)Facing;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(InteractPoint(), 0.25f);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 0.25f);
    }

    void Start()
    {
        TransformBaseOffset = spriteTransform.localPosition.y;
        interactableNameDisplay = FindObjectOfType<InteractableNameDisplay>();

        foreach (var go in GameObject.FindGameObjectsWithTag("Room")) go.SetActive(false);
        CurrentRoom.SetActive(true);
    }
   

    // Update is called once per frame
    void Update()
    {
        if (!MayTakeAction()) return;
        if (Input.GetKeyDown(KeyCode.Z))
        {
            InventoryManager.Instance.Open();
            return;
        }

        
        HighlightInteractableObject(CurrentInteractableItem());

        Movement();
    
        var hit = Physics2D.OverlapPoint(transform.position, HotspotLayerMask);
        if (hit != null)
        {
            foreach (var hs in hit.GetComponents<IHotspot>())
            {
                hs.Trigger(this);
            }
        }
    }
    public IInteractable CurrentInteractableItem()
    {
        IInteractable currentInteractable = null;
        var InteractableColliders = Physics2D.OverlapPointAll(InteractPoint(), InteractablesMask);

        if (InteractableColliders.Count() > 0)
        {
            var InteractableCollider = InteractableColliders.OrderBy(c => c.transform.position.y).First();
            currentInteractable = InteractableCollider.GetComponent<IInteractable>();
        }

        return currentInteractable;
    }

    IInteractable InteractableFromLastFrame;
    void HighlightInteractableObject(IInteractable currentInteractable) {
        if (InteractableFromLastFrame != null && InteractableFromLastFrame != currentInteractable)
        {
            InteractableFromLastFrame.SetState(false);
        }
        if (currentInteractable != null)
        {
            InteractableFromLastFrame = currentInteractable;
            currentInteractable.SetState(true);
            interactableNameDisplay.SetText(LocalizationManager.instance.GetLocalizedValue(currentInteractable.DisplayString()));
            if (Input.GetKeyDown(KeyCode.X))
            {
                currentInteractable.Interact();
            }
        }
        else
        {
            interactableNameDisplay.Hide();
        }

    }

    void Movement() {
        Vector3 InputDirection = new Vector3(playerInput.Horizontal(), playerInput.Vertical());
        if (InputDirection != Vector3.zero)
        {
            InputDirection.Normalize();
            Facing = (Vector2)LockToCardinals(InputDirection);

            for (int i = 0; i < 4; i += 1)
            {
                TryMovement(InputDirection.x * Vector3.right * Time.deltaTime * WalkSpeed / 4f);
                TryMovement(InputDirection.y * Vector3.up * Time.deltaTime * WalkSpeed / 4f);
            }
            anim.SetBool("Moving", true);
        }
        else
        {
            anim.SetBool("Moving", false);
        }
        if (Mathf.Abs(Facing.x) > 0.1f)
            anim.SetFloat("Facing", Facing.x);
    }


    static Vector3[] CardinalDirections = new Vector3[] {
        Vector3.up,
        -Vector3.up,
        Vector3.right,
        -Vector3.right,
    };
    public GameObject CurrentRoom;

    Vector3 LockToCardinals(Vector3 inp) {
        return CardinalDirections.OrderBy(c => Vector3.Angle(c, inp)).First();
    }

    void TryMovement(Vector3 direction)
    {
        if (direction == Vector3.zero) return;
        var NormalizeDirection = direction.normalized;
        var MoveDistance = direction.magnitude + 0.25f;
        var RaycastDistance = MoveDistance;

        foreach(var v2 in new Vector3[] {
            NormalizeDirection.yx() * 0.23f,
            -NormalizeDirection.yx() * 0.23f,
        }) {
            var hit = Physics2D.Raycast(transform.position + v2, NormalizeDirection, RaycastDistance, WallLayerMask);
			if (hit.collider != null)
			{
                if (MoveDistance > hit.distance) {
					MoveDistance = hit.distance; 
                }
			}
        }
        transform.Translate(NormalizeDirection * Mathf.Max(0f, MoveDistance-0.25f));
    }
}

