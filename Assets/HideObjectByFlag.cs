﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObjectByFlag : MonoBehaviour
{
    public string Flag;
    public bool HideIfSet;
    public GameObject Target;

    // Update is called once per frame
    void Update()
    {
        Target.SetActive(DataContainer.Instance.HasFlag(Flag) != HideIfSet);
    }
}
