﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toggleable : BaseInteractable
{
    public string ShowWhenOpeningKey;
    public string ShowWhenClosingKey;
    
    public override void Interact()
    {
        if (anim.GetBool("Open"))
        {
            // closing the firdge;
            anim.SetBool("Open", false);
            TextboxController.DisplayTranslation(ShowWhenClosingKey);
        }
        else
        {
            anim.SetBool("Open", true);
            TextboxController.DisplayTranslation(ShowWhenOpeningKey);
        }
    }
}
