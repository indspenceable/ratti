﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableNameDisplay : MonoBehaviour
{
    public TMPro.TextMeshProUGUI textDisplay;
    public TMPro.TextMeshProUGUI InvisibleTextDisplay;
    public GameObject Container;

    public void SetText(string t) {
        InvisibleTextDisplay.text = t;
        textDisplay.text = t;
        Container.SetActive(true);
    }
   public void Hide() {
        Container.SetActive(false);
    }
}
