﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstVisitText : MonoBehaviour
{
    [NaughtyAttributes.ReadOnly]
    public string UID = Guid.NewGuid().ToString();
    public string TranslationKey;
    private void Update()
    {
        if (!DataContainer.Instance.HasFlag(UID))
        {
            TextboxController.DisplayTranslation(TranslationKey);
            DataContainer.Instance.SetFlag(UID, true);
        }
    }
}
