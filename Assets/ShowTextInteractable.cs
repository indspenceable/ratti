﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTextInteractable : BaseInteractable
{
    public string TextToDisplayKey;
    public override void Interact()
    {
        base.Interact();
        TextboxController.DisplayTranslation(TextToDisplayKey);
    }
}
