﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Data/Item", order =1 )]
public class InventoryItem : ScriptableObject
{
    public Sprite inventorySprite;
    public string InventoryDisplayNameKey;
    public string OnPickupKey;
    [NaughtyAttributes.ReadOnly]
    public string UID = Guid.NewGuid().ToString();

 public void OnValidate()
    {
        if (UID == null || UID == "")
        {
            UID = Guid.NewGuid().ToString();
        }
    }
}
