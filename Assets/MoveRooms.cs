﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHotspot
{
    void Trigger(PlayerMovement pl);
}

public class MoveRooms : MonoBehaviour, IHotspot
{
    public Transform TargetPosition;

    public void Trigger(PlayerMovement pl)
    {
        pl.CurrentRoom.SetActive(false);
        pl.transform.position = TargetPosition.position;
        var TargetRoom = FindRoomInParents(TargetPosition);
        pl.CurrentRoom = TargetRoom;
        pl.CurrentRoom.SetActive(true);
        Camera.main.transform.position = TargetRoom.transform.position + Vector3.forward * -10;
    }

    void OnDrawGizmos()
    {
        if (TargetPosition == null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, 0.25f);
            return;
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, TargetPosition.position);
    }

    GameObject FindRoomInParents(Transform t)
    {
        var c = TargetPosition;
        while (true)
        {
            if (c.gameObject.CompareTag("Room"))
            {
                return c.gameObject;
            }
            c = c.parent;
        }
    }
}
