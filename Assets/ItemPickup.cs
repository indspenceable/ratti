﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : BaseInteractable
{
    public InventoryItem ItemToGet;
    public string FlagName;

    void Update()
    {
       if (DataContainer.Instance.HasFlag(FlagName))
        {
            gameObject.SetActive(false);
        }
    }

    public override void Interact()
    {
        DataContainer.Instance.SetFlag(FlagName, true);
        TextboxController.DisplayTranslation(ItemToGet.OnPickupKey);
        InventoryManager.Instance.Pickup(ItemToGet);
    }
    public override string DisplayString()
    {
        return ItemToGet.InventoryDisplayNameKey;
    }
}
