﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObjectsByFlag : MonoBehaviour
{
    public DataContainer Data;
    public string Flag;
    public GameObject FlagOn;
    public GameObject FlagOff;

    GameObject Current()
    {
        return Data.HasFlag(Flag) ? FlagOn : FlagOff;
    }
    GameObject InactiveInteractable()
    {
        return Data.HasFlag(Flag) ? FlagOff : FlagOn;
    }

    void Update()
    {
        Current().SetActive(true);
        InactiveInteractable().SetActive(false);
    }
}
