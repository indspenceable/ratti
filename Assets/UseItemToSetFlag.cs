﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseItemToSetFlag : BaseInteractable
{
    public DataContainer Data;
    public InventoryItem KeyItem;
    public string Flag;
    public string TextKey;

    public override void UseItem(string item)
    {
        if (item == this.KeyItem.UID)
        {
            TextboxController.DisplayTranslation(TextKey);
            Data.Inventory.Remove(item);
            Data.SetFlag(Flag, true);
        }
    }
}
