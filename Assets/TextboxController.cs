﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextboxController : MonoBehaviour
{
    public Transform container;
    public TMPro.TextMeshProUGUI VisibleTextBox;
    public TMPro.TextMeshProUGUI InvisibleTextBox;

    public bool CurrentlyDisplaying
    {
        get;
        private set;
    }

    private static TextboxController _Instance;
    public static TextboxController Instance {
        get {
            return _Instance;
        }
    }
    private void Start()
    {
        _Instance = this;
        CurrentlyDisplaying = false;
    }

    public static void DisplayTranslation(string key)
    {
        Instance.CurrentlyDisplaying = true;
        Instance.StartCoroutine(Instance.DisplayTranslationCo(key));
    }

    public IEnumerator DisplayTranslationCo(string key) {
        Time.timeScale = 0f;
        yield return OpenContainer(LocalizationManager.instance.GetLocalizedValue(key));
    }

    private IEnumerator OpenContainer(string s) {
        if (s == "")
        {
            container.gameObject.SetActive(false);
            Time.timeScale = 1f;
            CurrentlyDisplaying = false;
            yield break;
        }

        VisibleTextBox.text = "";
        container.localScale = new Vector3(1f, 0f, 1f);
        container.gameObject.SetActive(true);
        float duration = 0.1f;
        float dt = 0f;
        while (dt < duration) {
            yield return null;
            dt += Time.unscaledDeltaTime;
            container.localScale = new Vector3(1f, Mathf.Clamp01(dt/duration), 1f);
        }
        yield return _Display(s, true);
        
    }

    private IEnumerator _Display(string text, bool CloseAtEnd) {
        if (text.Contains("\n"))
        {
            int i = text.IndexOf("\n");
            yield return _Display(text.Substring(0, i), false);
            yield return _Display(text.Substring(i+1), true);
        }
        else
        {

            InvisibleTextBox.text = text;
            InvisibleTextBox.ForceMeshUpdate();

            if (InvisibleTextBox.isTextOverflowing)
            {
                int i = InvisibleTextBox.firstOverflowCharacterIndex;

                yield return DisplayPage(text.Substring(0, i), text.Substring(i), CloseAtEnd);
            }
            else
            {
                // just assume we have less than one page.
                yield return DisplayPage(text, "", CloseAtEnd);
            }
        }
    }

    private IEnumerator DisplayPage(string text, string RemainingText, bool CloseAtEnd)
    {

        var wfs = new WaitForSecondsRealtime(0.1f);
        int lettersSinceWait = 0;
        yield return new WaitForEndOfFrame();
        bool XPress = false;
        for (int i = 0; i <= text.Length; i += 1)
        {

            string visiblePortion = text.Substring(0, i);
            string invisiblePortion = text.Substring(i);
            string CompositeString = visiblePortion + "<color=#FFFFFF00>" + invisiblePortion + "</color>";
            VisibleTextBox.text = CompositeString;
            lettersSinceWait += 1;
            XPress = Input.GetKeyDown(KeyCode.X) || XPress;
            if (!XPress)
            {
                float waitDuration = 0.1f;
                float dt = 0f;
                while (dt < waitDuration)
                {
                    yield return null;
                    dt += Time.unscaledDeltaTime;
                    XPress = Input.GetKeyDown(KeyCode.X) || XPress;
                }
                lettersSinceWait = 0;
            }
        }
        yield return new WaitForEndOfFrame();
        while (!Input.GetKeyDown(KeyCode.X))
            yield return null;
        yield return new WaitForEndOfFrame();

        if (RemainingText == "") {
            if (CloseAtEnd)
            {
                container.gameObject.SetActive(false);
                Time.timeScale = 1f;
                CurrentlyDisplaying = false;
            }
        } else {
            yield return _Display(RemainingText, CloseAtEnd);
        }
    }
}
