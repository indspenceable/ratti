﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToPixel : MonoBehaviour
{
    public float XSnap = 1f / 16f;
    public float YSnap = 1f / 16f;
    public float ZSnap = 1f / 16f;

    public Vector3 Offset;

    public void LateUpdate()
    {
        transform.localPosition = Offset;
        float x = Mathf.RoundToInt(transform.position.x / XSnap) * XSnap;
        float y = Mathf.RoundToInt(transform.position.y / YSnap) * YSnap;
        float z = Mathf.RoundToInt(transform.position.z / ZSnap) * ZSnap;
        transform.localPosition += new Vector3(transform.position.x - x,
            transform.position.y - y,
            transform.position.z - z);
    }
}
