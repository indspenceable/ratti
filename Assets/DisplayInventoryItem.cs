﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayInventoryItem : MonoBehaviour
{
    public DataContainer Data;
    public int SelectionOffset;
    public Image image;
    public InventoryManager inventoryManager;

    private void LateUpdate()
    {
        ShowCurrentItem();
    }
    public void ShowCurrentItem() { 
        if (Data.Inventory.Count == 0)
        {
            ShowNothing();
        }
        else if (Data.Inventory.Count == 1 && SelectionOffset != 0)
        {
            ShowNothing();
        }
        else
        {
            int myOffset = (SelectionOffset + Data.Inventory.Count + inventoryManager.currentPosition) % Data.Inventory.Count;
            ShowItem(Data.Inventory[myOffset]);
        }
    }

    void ShowItem(string UID)
    {
        image.sprite = Data.AllItems.Find(i => i.UID == UID).inventorySprite;
        image.enabled = true;
    }
    void ShowNothing()
    {
        image.sprite = null;
        image.enabled = false;
    }
}
