﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class MatchPPCScale : MonoBehaviour
{
    public CanvasScaler scaler;
    public Canvas Renderer;
    private PixelPerfectCamera PPC;
    public float scale = 0.25f;

    private IEnumerator Start()
    {
        PPC = Camera.main.GetComponent<PixelPerfectCamera>();
        LateUpdate();
        Renderer.worldCamera = Camera.main; 
        yield return new WaitForEndOfFrame();
        Renderer.worldCamera = Camera.main;
        Renderer.enabled = true;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        scaler.scaleFactor = scale * PPC.pixelRatio;
    }
}
