﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectInteractable : BaseInteractable
{
    public string Flag;
    public Transform Destination;
    public string DisplayOnMoveKey;
    public string DisplayAfterKey;

    // Start is called before the first frame update
    void Start()
    {
        if (DataContainer.Instance.HasFlag(Flag))
            transform.position = Destination.position;
    }
    public override void Interact()
    {
        if (DataContainer.Instance.HasFlag(Flag))
        {
            TextboxController.DisplayTranslation(DisplayAfterKey);
        } else
        {
            TextboxController.DisplayTranslation(DisplayOnMoveKey);
            DataContainer.Instance.SetFlag(Flag, true);
            transform.position = Destination.position;
        }
    }
}
