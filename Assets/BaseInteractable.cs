﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseInteractable : MonoBehaviour, IInteractable
{
    public Animator anim;
    public string DisplayKey;
    public GameObject ButtonIndicator;

    private void Start()
    {
        SetState(false);
    }
    public void SetState(bool v)
    {
        if (anim != null) anim.SetBool("On", v);
        if (ButtonIndicator != null) ButtonIndicator.SetActive(v);
    }

    public virtual string DisplayString()
    {
        return DisplayKey;
    }
    public virtual void Interact() { }

    public virtual void UseItem(string item)
    {
        TextboxController.DisplayTranslation("general.no_effect");
        InventoryManager.Instance.Close();
    }
}
