﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public int currentPosition;
    public DataContainer Data;
    public GameObject Container;
    public string NoEffectKey;

    private static InventoryManager _Instance;
    public static InventoryManager Instance
    {
        get
        {
            return _Instance;
        }
    }
    private void Start()
    {
        _Instance = this;
        Close();
    }
    public bool IsOpen()
    {
        return Container.activeSelf;
    }

    internal void Pickup(InventoryItem ItemToGet)
    {
        DataContainer.Instance.GetItem(ItemToGet);
        currentPosition = DataContainer.Instance.Inventory.Count - 1;
    }

    public void Open()
    {
        Invoke("_DoOpen", 0);
    }
    void _DoOpen()
    {
        Container.SetActive(true);
    }
    public void Close()
    {
        Container.SetActive(false);
    }

    private void Update()
    {
        if (!IsOpen()) return;
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            currentPosition -= 1;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            currentPosition += 1;
        }

        if (Data.Inventory.Count <= 1) currentPosition = 0;
        else currentPosition = (Data.Inventory.Count + currentPosition) % Data.Inventory.Count;

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Invoke("Close", 0);
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            if (Data.Inventory.Count > 0)
            {
                var item = Data.Inventory[currentPosition];
                var ii = FindObjectOfType<PlayerMovement>().CurrentInteractableItem();
                if (ii != null)
                {
                    ii.UseItem(item);
               
                }
                else
                {
                    TextboxController.DisplayTranslation(NoEffectKey);
                }
                InventoryManager.Instance.Close();
            }
        }
    }
}
