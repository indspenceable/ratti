﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataContainer : MonoBehaviour
{
    public List<InventoryItem> AllItems;
    public List<string> Inventory;
    public List<string> Flags;

    public bool HasFlag(string s)
    {
        return Flags.Contains(s);
    }
    public void SetFlag(string s, bool f)
    {
        if (HasFlag(s) != f)
        {
            if (f) Flags.Add(s);
            else Flags.Remove(s);
        }
    }

    public void GetItem(InventoryItem ii)
    {
        var uid = ii.UID;
        Inventory.Add(uid);
    }
    public void RemoveItem(InventoryItem ii)
    {
        var uid = ii.UID;
        Inventory.Remove(uid);
    }

    private static DataContainer _Instance;
    public static DataContainer Instance
    {
        get
        {
            return _Instance;
        }
    }
    private void Start()
    {
        _Instance = this;
    }
}
